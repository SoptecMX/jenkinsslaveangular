FROM jenkins/slave:3.16-1
MAINTAINER Brandom Marañon <brandommario@gmail.com>

COPY jenkins-slave /usr/local/bin/jenkins-slave

ENV NODE_VERSION=6.11.4 \
    YARN_VERSION=1.3.2 \
    ANGULAR_CLI_VERSION=1.5.2 \
    GULP_VERSION=3.9.1 \
    CORDOVA_VERSION=6.3.1 \
    MAVEN_VERSION=3.5.0

USER root
RUN apt-get update \
    && apt-get install -y zip openjdk-8-jdk \
    && cd /opt/ \
    && wget https://repo.maven.apache.org/maven2/org/apache/maven/apache-maven/${MAVEN_VERSION}/apache-maven-${MAVEN_VERSION}-bin.zip \
    && unzip apache-maven-${MAVEN_VERSION}-bin.zip \
    && mv apache-maven-${MAVEN_VERSION}-bin maven \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

USER jenkinsso
RUN echo "export M2_HOME=/opt/maven" >> $HOME/.bashrc \
        && echo "export PATH=${M2_HOME}/bin:${PATH}" >> $HOME/.bashrc \
        && curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash \
        && export NVM_DIR="$HOME/.nvm" \
        && [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" \
        && [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" \
        && nvm install $NODE_VERSION \
        && nvm use $NODE_VERSION \
        && nvm alias default $NODE_VERSION \
        && echo "export NVM_DIR="$HOME/.nvm"" >> $HOME/.bashrc \
        && echo "[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"" >> $HOME/.bashrc \
        && echo "[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"" >> $HOME/.bashrc \
        && npm install -g @angular-devkit/core@0.0.29 \
        && npm install -g @angular/cli@${ANGULAR_CLI_VERSION} \
        && npm install -g yarn@$YARN_VERSION \
        && npm install -g gulp-cli \
        && npm install -g gulp@${GULP_VERSION} \
        && npm install -g cordova@${CORDOVA_VERSION} \
        && npm cache clean \
        && nvm cache clear

ENTRYPOINT ["jenkins-slave"]
